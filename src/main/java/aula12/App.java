package aula12;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Stream;

public class App {
  public static void main(String[] args) throws SQLException {
    String url = "jdbc:h2:tcp://localhost:9092/~/h2/ht/aula12/aula12";
    String user = "sa";
    String password = "";
    Pessoa[] pessoas = {
        new Pessoa(1, "Ana"),
        new Pessoa(2, "Bruna"),
        new Pessoa(3, "Carla")
    };
    String query = "SELECT ID, NOME FROM PESSOA";

    Connection con = null;
    try {
      con = DriverManager.getConnection(url, user, password);
      con.setAutoCommit(false);
      try (Statement st = con.createStatement();
          ResultSet rs = st.executeQuery(query)) {// Java 7 - declarando variáveis AutoClosable

        st.executeUpdate("delete from pessoa");
        String insert = "insert into pessoa (id, nome) values (%d, '%s')";
        Stream.of(pessoas)
            .map(p -> String.format(insert, p.id, p.nome))
            .forEach(s -> {
              try {
                st.executeUpdate(s);
              } catch (SQLException e) {
                throw new RuntimeException();
              }
            });
        con.commit();

        /* DDL da tabela */
        // st.execute("CREATE TABLE PESSOA("
        // + "ID INTEGER NOT NULL,"
        // + "CONSTRAINT PK_PESSOA PRIMARY KEY (ID),"
        // + "NOME VARCHAR_IGNORECASE(50) NOT NULL);");
        // con.commit();

        /* Insert elegante */
        // String insert = "INSERT INTO PESSOA (ID, NOME) VALUES (%d, '%s')";
        // Stream.of(pessoas)
        // .map(p -> String.format(insert, p.id, p.nome))
        // .forEach(i -> {
        // try {
        // st.executeUpdate(i);
        // } catch (SQLException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
        // });

        /* Execute query */
        while (rs.next())
          System.out.println(rs.getInt("ID") + " " + rs.getString("NOME"));

        con.commit();

      }
    } catch (SQLException e) {
      try {
        if (con != null)
          con.rollback();
      } catch (SQLException e1) {
        e1.printStackTrace();
      } finally {
        con.close();
      }
    }

  }
}

class Pessoa {
  int id;
  String nome;

  public Pessoa(int id, String nome) {
    super();
    this.id = id;
    this.nome = nome;
  }

}
